<?php


namespace Esol\AdminBundle\Event;


class AdminBundleSideBarConfigurationEvent
{

    private $configuration;

    /**
     * AdminBundleSideBarConfigurationEvent constructor.
     * @param $configuration
     */
    public function __construct()
    {
        $this->configuration = array();
    }

    public function addConfiguration(array $config){
        $this->configuration[$config['bundleName']] = $config;
    }

    public function getConfiguration(){
        return $this->configuration;
    }


}