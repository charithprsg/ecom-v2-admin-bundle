<?php


namespace Esol\AdminBundle\Event;


class AdminBundleEvents
{
    /**
     * @Event("Esol\AdminBundle\Event\AdminBundleSideBarConfigurationEvent")
     */
    const CONFIG = "esol.side-config";
}