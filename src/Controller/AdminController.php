<?php


namespace Esol\AdminBundle\Controller;


use Esol\LocationBundle\Listener\LocationBundleConfigurationListener;
use Esol\CartBundle\Listener\CartBundleConfigurationListener;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Esol\AdminBundle\Event\AdminBundleEvents;
use Esol\AdminBundle\Event\AdminBundleSideBarConfigurationEvent;

class AdminController extends AbstractController
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;


    /**
     * AdminController constructor.
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getTemplate(Request $request){
        return $this->render('@EsolAdmin/admin_base.html.twig');
    }

    public function getSideBar(Request $request){
        $event = new AdminBundleSideBarConfigurationEvent();
        $this->eventDispatcher->dispatch($event,AdminBundleEvents::CONFIG);
        $configs = $event->getConfiguration();
        return $this->render('@EsolAdmin/sidebar.html.twig',array('configs'=>$configs));
    }

}